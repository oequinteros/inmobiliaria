package inmobiliaria;

public class Automovil {

    private String patente;
    private Modelo modelo;

    public Automovil(String patente, Modelo modelo){
        this.patente=patente;
        this.modelo=modelo;
    }
    public String toString(){
        return "Mi Automovil";
    }
    
}
