package inmobiliaria;

public class Modelo {
    private String descripcion;
    private Marca marca;
    
    public Modelo(String descripcion, Marca marca){
        this.descripcion=descripcion;
        this.marca=marca;
    }
    
    public String toString(){
        return "Mi Modelo";
    }    
}
