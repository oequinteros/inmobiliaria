package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AutomovilTest {
    
    @Test
    public void verificarRetornoToString(){
        Marca marca = new Marca("FORD");
        Modelo modelo = new Modelo("K", marca);
        Automovil automovil = new Automovil("AA999AA", modelo);

        String mensaje = automovil.toString();

        assertEquals("Mi Automovil", mensaje);
    }
}
