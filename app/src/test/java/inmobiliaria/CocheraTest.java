package inmobiliaria;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CocheraTest {
    
    @Test
    public void agregarUnAutomovilAlaCochera(){
        Marca marca = new Marca("FORD");
        Modelo modelo = new Modelo("K", marca);
        Automovil auto = new Automovil("AA123AA",modelo);
        Cochera cochera = new Cochera();

        cochera.agregarAutomovil(auto);

        assertEquals(1, cochera.cantidadAutos());

    }
    @Test
    public void sacarUnAutomovilAlaCochera(){
        Marca marca = new Marca("FORD");
        Modelo modelo = new Modelo("K", marca);
        Automovil auto1 = new Automovil("AA123AA",modelo);
        Automovil auto2 = new Automovil("BB123AA",modelo);
        Cochera cochera = new Cochera();
        cochera.agregarAutomovil(auto1);
        cochera.agregarAutomovil(auto2);


        cochera.sacarAutomovil(auto1);


        assertEquals(1, cochera.cantidadAutos());

    }

}
